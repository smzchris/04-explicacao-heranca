
public class Aluno extends Pessoa{
	private String ra;
	
	public Aluno(String nome, String cpf, String ra) {
		super(nome, cpf);
		this.ra = ra;
	}
	
	public String getRa() {
		return ra;
	}
	
	@Override
	public String toString() {
		return nome + " " + cpf + " " + ra;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

}
